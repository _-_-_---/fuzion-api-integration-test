# Fuzion API Integration Test

place required env vars in a `.env` file
```
FUZION_API_KEY=...
FUZION_API_SECRET_KEY=...
APPLICATION_ID=...
```

`fuzionHeaders.js` generates necessary headers for a request required to make API calls.

`npm start` to run example request

Based on documentation from: https://fuziondeveloper.com/index.html#tag/Requests
