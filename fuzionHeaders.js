const crypto = require('crypto');
require('dotenv').config();

module.exports = (
  {
    hostname = 'fuzionapi.com',
    path,
    method = 'GET',
    fuzionApiKey = process.env.FUZION_API_KEY,
    fuzionApiSecretKey = process.env.FUZION_API_SECRET_KEY,
    appId = process.env.APPLICATION_ID,
  }
) => {
  const requestTimestamp = new Date().valueOf();
  const signatureParts = `${hostname}${path}|${method}|${requestTimestamp}|${fuzionApiKey}`;

  // Create signature using signatureParts and secretKey
  const partnerAppSignature = crypto.createHmac("sha256", fuzionApiSecretKey)
  .update(signatureParts)
  .digest("base64");

  const fuzionHeaders = {
    partner_app_key: fuzionApiKey,
    partner_app_signature: partnerAppSignature,
    fuzion_event_id: appId,
    request_timestamp: requestTimestamp,
  }

  return {
    fuzionHeaders
  }
}
