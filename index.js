const https = require('https');
const fuzion = require('./fuzionHeaders');

// generate fuzion api headers including signature
const hostname = 'fuzionapi.com';
const path = '/v1/exhibitors';
const method = 'GET';
const { fuzionHeaders } = fuzion({ hostname, path, method });

const headers = { ...fuzionHeaders, page_size: 500 };

const requestOptions = {
  hostname,
  path,
  method,
  headers,
};

const req = https.request(
  requestOptions,
  res => {
    res.on('data', d => {
      process.stdout.write(d)
    })
  },
);

req.on('error', error => {
  console.error(error)
});

req.end();
